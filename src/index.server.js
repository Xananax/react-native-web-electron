import { readFileSync } from 'fs';
import ReactDOMServer from 'react-dom/server';
import { AppRegistry } from 'react-native-web';
import App from './src/App';
import config from '../package.json'

const title = process.env.TITLE || config.name

const pageText = readFileSync('../public/index.html',{ encoding:'utf8'})
// register the app
AppRegistry.registerComponent('App', () => App);

// prerender the app
const { element, getStyleElement } = AppRegistry.getApplication('App', { initialProps });
// first the element
const html = ReactDOMServer.renderToString(element);
// then the styles (optionally include a nonce if your CSP policy requires it)
const css = ReactDOMServer.renderToStaticMarkup(getStyleElement({ nonce }));

// example HTML document string
const document = pageText
    .replace(/<title.*?\/title>/i,`<title>${title}</title>`)
    .replace(/<\/head>/i,`${css}</head>`)
    .replace(/<div id="root".*?\/div>/i,`<div id="root">${html}</div>`)

export const document;
export default document