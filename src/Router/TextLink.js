import React from 'react';
import { Link } from "./Routing";
import { Text, TouchableOpacity } from "react-native";

export const TextLink = ({ to, style, children }) => (
  <TouchableOpacity>
    <Link to={to}>
      <Text style={style}>{children}</Text>
    </Link>
  </TouchableOpacity>
);

export default TextLink;
