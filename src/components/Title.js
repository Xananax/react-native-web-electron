import React from 'react'
import { Platform } from 'react-native';

const isWeb = (Platform.OS === 'web')

export class Title extends React.Component {
  componentWillUpdate(nextProps) {
    if(!isWeb){ return }
    const { global } = Title
    const { children } = this.props
    const title = global ? `${children} | ${global}` : `${children}`
    document.title = title;
  }
  render(){
    return null;
  }
}

export default Title