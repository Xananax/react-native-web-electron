import React from "react";
import { StyleSheet, View } from "react-native";
import Title from './Title'

export const Page = ({ title, children }) => (
  <View>
      <Title>{title}</Title>
      { children }
  </View>
);


export default Page