import React from "react";
import { Button, Image, StyleSheet, Text, View } from "react-native";
import { ExternalLink } from "../Router";
import { styles } from '../styles'
import Page from './Page'

export const Home = () => (
  <Page title="Home">
    <Text style={styles.text}>
      This is an example of an app built with{" "}
      <ExternalLink href="https://github.com/facebook/create-react-app">
        Create React App
      </ExternalLink>{" "}
      and{" "}
      <ExternalLink href="https://github.com/necolas/react-native-web">
        React Native for Web
      </ExternalLink>
    </Text>
    <Text style={styles.text}>
      To get started, edit{" "}
      <ExternalLink
        href="https://codesandbox.io/s/q4qymyp2l6/"
        style={styles.code}
      >
        src/App.js
      </ExternalLink>
      .
    </Text>
    <Button onPress={() => {}} title="Example button" />
  </Page>
);


export default Home