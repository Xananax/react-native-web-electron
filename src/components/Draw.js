import React from 'react'
import { View, PanResponder, Button } from 'react-native'
import { Group, Shape, Surface, Path } from 'react-art';
import Icon from '../Icon';

const makePath = (points, arc, close) => {
  if(!points || !points.length){ return null }
  const { length } = points
  const firstPoint = points[0]
  const { x:_x, y:_y } = firstPoint
  let i = 1;
  let path = Path().moveTo(_x, _y)
  for(i;i<length;i++){
    const { x, y } = points[i]
    path = arc ? path.arcTo(x,y) : path.lineTo(x,y)
  }
  if(close){
    path = arc ? path.arcTo(_x,_y) : path.lineTo(_x,_y)
  }
  return path
}

const Drawing = ({ points_list, color, strokeWidth, curve, close }) => {
  const path = makePath(points_list,curve,close)
  if(!path){ return null }
  return (
    <Group>
      <Shape stroke={color} strokeWidth={strokeWidth} d={path} />
    </Group>
  );
}

const returnTrue = () => true

export class Draw extends React.Component{

  state = {
    mouseIsDown:false,
    next_shape: [],
    mode:'pencil',
    shapes_list:[
      {
        color:'#000000',
        points_list:[
          {x:0,y:0},
          {x:100,y:0},
          {x:100,y:100},
          {x:0,y:100}
        ]
      }
    ]
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: returnTrue,
      onMoveShouldSetPanResponder: returnTrue,
      onPanResponderGrant: this.onMouseDown,
      onPanResponderMove: this.onMouseMove,
      onPanResponderRelease: this.onMouseUp,
      onPanResponderTerminate: this.onMouseUp
    });
  }

  generateNextShape(){
    if(!this.state.next_shape.length){
      return null
    }
    const { mode } = this.state
    const color = mode === 'eraser' ? '#ffffff' : '#000000'
    const strokeWidth = mode === 'eraser' ? 7 : 3
    return { color, strokeWidth, points_list:this.state.next_shape }
  }

  onMouseDown = () => {
    this.setState({ mouseIsDown:true })
  }
  
  onMouseUp = () => {
    const mouseIsDown = false
    if(this.state.next_shape.length){
      const next_shape = this.generateNextShape()
      if(!next_shape){ return }
      const shapes_list = [ ...this.state.shapes_list, next_shape ]
      const reset_next_shape = []
      this.setState({ shapes_list, next_shape:reset_next_shape, mouseIsDown })
    }else{
      this.setState({ mouseIsDown })
    }
  }

  onMouseMove = (evt, gestureState) => {
    const { locationX:x, locationY:y } = evt.nativeEvent
    const point = { x, y }
    const next_shape = [...this.state.next_shape, point]
    this.setState({ next_shape })
  }

  onPenClick = () => this.setState({ mode:'pencil' })
  
  onEraserClick = () => this.setState({ mode:'eraser' })

  render() {
    const { mode } = this.state
    const next_shape = this.generateNextShape()
    const all_shapes = next_shape ? [...this.state.shapes_list,  next_shape ] : this.state.shapes_list
    return (
      <View {...this._panResponder.panHandlers}>
        <View>
          <Icon.Button name="pencil" size={30} backgroundColor={mode==='pencil' ? '#3b5998' : '#fb59ff'} onPress={this.onPenClick}/>
          <Icon.Button name="eraser" size={30} backgroundColor={mode==='eraser' ? '#3b5998' : '#fb59ff'} onPress={this.onEraserClick}/>
        </View>
        <Surface
          width={700}
          height={700}
          style={{cursor: 'pointer'}}>
          {
            all_shapes.map( ({ points_list, color, strokeWidth }, i)=><Drawing color={color} strokeWidth={strokeWidth} points_list={points_list} key={i}/>)
          }
        </Surface>
      </View>
    );
  }

}

export default Draw;

