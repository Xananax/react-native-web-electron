# React Native Starter


Installed those:

```sh
yarn add react react-dom react-native-web react-art @babel/core @babel/cli @babel/preset-env @babel/preset-react babel-plugin-module-resolver electron webpack webpack-cli webpack-dev-server style-loader css-loader babel-loader react-hot-loader react-router axios react-native-vector-icons @babel/plugin-proposal-class-properties resize-observer-polyfill babel-plugin-react-native-web react-router-dom url-loader metro-react-native-babel-preset @babel/runtime
```

Made a babel and webpack config

Wrote some code

## Scripts

`yarn start` to debug

`yarn web` to build the website
